package org.springframework.samples.petclinic;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.samples.petclinic.vet.VetRepository;

@SpringBootTest
public class PetclinicIntegrationTests {

	@Autowired
	private VetRepository vets;

	@Test
	void testFindAll() {
		vets.findAll();
		vets.findAll(); // served from cache
	}

}
